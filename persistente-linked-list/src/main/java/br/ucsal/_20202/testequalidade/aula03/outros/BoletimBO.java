package br.ucsal._20202.testequalidade.aula03.outros;

public class BoletimBO {

	public static String definirSituacao(Double media) {
		if (media > 10) {
			return "Média inválida";
		}
		if (media >= 6) {
			return "Aprovado";
		}
		if (media >= 3) {
			return "Prova final";
		}
		if (media >= 0) {
			return "Reprovado";
		}
		return "Média inválida";
	}

}
