package br.ucsal._20202.testequalidade.aula03.outros;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AlunoTest {

	@Test
	void testarClonagem() {
		Aluno aluno = new Aluno(123, "Claudio Neiva", "Rua x");

		Aluno alunoAtual = aluno.clonar();

		Assertions.assertEquals(aluno, alunoAtual);
		Assertions.assertNotSame(aluno, alunoAtual);
		// Assertions.assertNotSame(aluno, alunoAtual, () -> "mensagem gerada");
		// Assertions.assertNotSame(aluno, alunoAtual, "Não é correto que o clone seja a
		// mesma instância do objeto clonado");
	}

}
