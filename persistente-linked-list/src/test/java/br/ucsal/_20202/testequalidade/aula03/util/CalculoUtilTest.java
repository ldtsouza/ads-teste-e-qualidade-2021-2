package br.ucsal._20202.testequalidade.aula03.util;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import br.ucsal._20202.testequalidade.aula03.outros.Aluno;

class CalculoUtilTest {

	private CalculoUtil calculoUtil;

	@BeforeEach
	void setup() {
		calculoUtil = new CalculoUtil();
	}

	@Test
	void testarFatorial0() {
		// Definir dados de entrada
		int n = 0;
		// Definir a saída esperada
		Long fatorialEsperado = 1L;

		// Executar o método que está sendo testado
		// Obter a saída a atual (saída que o método que sendo testado apresentou)
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		assertEquals(fatorialEsperado, fatorialAtual);
	}

	@Test
	void testarFatorial5() {
		// Definir dados de entrada
		int n = 5;
		// Definir a saída esperada
		long fatorialEsperado = 120;

		// Executar o método que está sendo testado
		// Obter a saída a atual (saída que o método que sendo testado apresentou)
		long fatorialAtual = calculoUtil.calcularFatorial(n);

		// Comparar a saída esperada com a saída atual
		assertEquals(fatorialEsperado, fatorialAtual);

	}

	@Test
	void testarFatorial0E1E5E6() {
		assertAll("Calcular fatoriais", () -> assertEquals(1, calculoUtil.calcularFatorial(0)),
				() -> assertEquals(1, calculoUtil.calcularFatorial(1)),
				() -> assertEquals(120, calculoUtil.calcularFatorial(5)),
				() -> assertEquals(720, calculoUtil.calcularFatorial(6)));
	}

	@ParameterizedTest
	@CsvSource({ "0,1", "3,6", "5,120", "6,720" })
	void testarFatoriais(int n, long fatorialEsperado) {
		long fatorialAtual = calculoUtil.calcularFatorial(n);
		assertEquals(fatorialEsperado, fatorialAtual);
	}
	
	@ParameterizedTest
	@MethodSource("fornecerAlunosTest")
	void testarCloneAlunos(Aluno aluno) {
		Aluno alunoClonado = aluno.clonar();
		Assertions.assertEquals(aluno, alunoClonado);
		Assertions.assertNotSame(aluno, alunoClonado);
	}
	
	private static Stream<Arguments> fornecerAlunosTest() {
		Aluno aluno1 = new Aluno(123, "Claudio Neiva", "Rua x");
		Aluno aluno2 = new Aluno(567, "Maria da Silva", null);
		Aluno aluno3 = new Aluno(789, "João", "");
	    return Stream.of(
  	      Arguments.of(aluno1),
	      Arguments.of(aluno2),
	      Arguments.of(aluno3)
	    );
	}

}
