package br.ucsal.bes20212.testequalidade.aula12;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class LancamentoNotaSteps {

	private Aluno aluno;

	@Given("um aluno está matriculado na disciplina")
	public void instanciarAluno() {
		aluno = new Aluno();
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		aluno.informarNota(nota);
	}

	@Then("a situação do aluno é $situacao")
	public void verificarSituacaoAluno(String situacaoEsperada) {
		String situacaoAtual = aluno.obterSituacao();
		Assert.assertEquals(situacaoEsperada, situacaoAtual);
	}

}
