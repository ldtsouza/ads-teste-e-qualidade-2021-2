package br.ucsal.bes20212.testequalidade.locadora.infra;

import java.util.List;

import br.ucsal.bes20212.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20212.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20212.testequalidade.locadora.persistence.VeiculoDAO;

public class VeiculoDAOStub extends VeiculoDAO {

	private List<Veiculo> veiculosDefinidos;

	public void definirVeiculos(List<Veiculo> veiculos) {
		this.veiculosDefinidos = veiculos;
	}
	
	@Override
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		return veiculosDefinidos;
	}

}
